from django.conf.urls import url
from .views import index

#url for app, add your URL Configuration
#https://pepewelab2.herokuapp.com/

urlpatterns = [
url(r'^$', index, name='index'),
]
